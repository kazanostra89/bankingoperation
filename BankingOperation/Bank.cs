﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace BankingOperation
{
    class Bank
    {
        private int clientID;
        private const string nameBank = "Bank of America";
        private Dictionary<int, Client> clientsBank;
        private Dictionary<int, StatusesCards> numbersCard;

        public Bank()
        {
            clientID = 0;
            clientsBank = new Dictionary<int, Client>();
            numbersCard = new Dictionary<int, StatusesCards>();
        }

        public void AddNewClient(string name, string surname, int deposit)
        {
            clientID += 1;

            clientsBank.Add(clientID, new Client(name, surname, deposit, clientID));

            PrintInfoClientByID(clientID);
        }

        public void RemoveClient(int clientID)
        {
            clientsBank.Remove(clientID);
        }

        public void SetInvalidStatusCard(int numberCard)
        {
            numbersCard[numberCard] = StatusesCards.Invalid;
        }

        public int GetNewNumberCard(Random rnd)
        {
            int numberNewCard;

            do
            {
                numberNewCard = rnd.Next(1000, 10000);

            } while (numbersCard.Keys.Contains(numberNewCard));

            numbersCard.Add(numberNewCard, StatusesCards.Valid);

            return numberNewCard;
        }

        public Client GetClientByID(int clientID)
        {
            return clientsBank[clientID];
        }

        public bool CheckClienByID(int clientID)
        {
            return clientsBank.Keys.Contains(clientID);
        }

        public bool CheckCardByNumber(int cardNumber)
        {
            return numbersCard.Keys.Contains(cardNumber);
        }

        public void SaveListCardsToFile()
        {
            StreamWriter writer = new StreamWriter(@"C:\Users\Родион\Desktop\123\ListCardDB.txt", false);

            foreach (KeyValuePair<int, StatusesCards> card in numbersCard)
            {
                writer.WriteLine(card.Key + "_" + card.Value);
            }

            writer.Close();
        }

        public void ExecListCardsFromFile()
        {
            StreamReader reader = new StreamReader(@"C:\Users\Родион\Desktop\123\ListCardDB.txt");

            while (!reader.EndOfStream)
            {
                string tempString = reader.ReadLine();
                string[] tempsString = tempString.Split('_');

                numbersCard.Add(Helper.ConvertToInt(tempsString[0]), Helper.ConverToStatusesCards(tempsString[1]));
            }

            reader.Close();
        }

        public void PrintClientsBankAllList()
        {
            foreach (KeyValuePair<int, Client> item in clientsBank)
            {
                Console.WriteLine(item.Value.PrintInfoClientString());
            }
        }

        public void PrintInfoClientByID(int clientID)
        {
            Console.WriteLine(clientsBank[clientID].PrintInfoClientString());
        }

        #region Процедуры для работы с коллекцией выпущенных банком карт
        public void PrintNumbersCardAllList()
        {
            Console.WriteLine("Список всех выданных банком номеров карт: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Недействительные карты помечены желтым цветом!\n");
            Console.ResetColor();
            Console.WriteLine("{0,-9}{1,-7}", "№ карты", "Статус");

            foreach (KeyValuePair<int, StatusesCards> item in numbersCard.OrderBy(item => item.Key))
            {
                if (item.Value == StatusesCards.Invalid)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                }

                Console.WriteLine("{0,-9}{1,-7}", item.Key, item.Value);
                Console.ResetColor();
            }

            Console.WriteLine();
        }

        public void PrintNumbersCardByValue(int cardNumber)
        {
            Console.WriteLine("{0,-9}{1,-7}", "№ карты", "Статус");

            foreach (KeyValuePair<int, StatusesCards> item in numbersCard)
            {
                if (item.Key == cardNumber)
                {
                    Console.WriteLine("{0,-9}{1,-7}", item.Key, item.Value);

                    //bool checkNumberCardForClient = clientsBank.Values.ToList().Exists(check => check.CheckCardByNumber(cardNumber));

                    if (item.Value == StatusesCards.Valid)
                    {
                        Client clientReport = clientsBank.Values.First(client => client.CheckCardByNumber(cardNumber));

                        Console.WriteLine("\nБаланс: " + clientReport.GetCardByNumber(cardNumber).BalanceCard);
                        Console.WriteLine("\nИнформация о владельце карты: ");
                        Console.WriteLine(clientReport.PrintInfoClientString());
                    }
                    else
                    {
                        Console.WriteLine("\nИнформация о клиенте и балансе отсутствует в БД, вероятно счет был закрыт!");
                    }
                }
            }
        }

        public void PrintNumbersCardByValue(StatusesCards status)
        {
            Console.WriteLine("{0,-9}{1,-7}", "№ карты", "Статус");

            foreach (KeyValuePair<int, StatusesCards> item in numbersCard.OrderBy(item => item.Key))
            {
                if (item.Value == status)
                {
                    Console.WriteLine("{0,-9}{1,-7}", item.Key, item.Value);
                }
            }
        }
        #endregion


        public string NameBank
        {
            get { return nameBank; }
        }

        public int CountClientsBank
        {
            get { return clientsBank.Count; }
        }

        public int CountNumberCards
        {
            get { return numbersCard.Count; }
        }

        

    }
}

﻿using System;

namespace BankingOperation
{
    class BankCard
    {
        private int cardNumber;
        private int cardBalance;

        public BankCard(int cardNumber, int cardBalance)
        {
            this.cardNumber = cardNumber;
            this.cardBalance = cardBalance;
        }

        public void CashOperation(int money, Remittance operation)
        {
            switch (operation)
            {
                case Remittance.Replenish: //пополнение счета
                    cardBalance += money;
                    break;
                case Remittance.Withdrawal: //снятие со счета
                    cardBalance -= money;
                    break;
            }
        }

        public string PrintInfoCardString()
        {
            return "Номер карты: " + cardNumber + "\nБаланс карты: " + cardBalance;
        }

        public void PrintCardByList()
        {
            Console.WriteLine("{0,-9}{1,-7}", cardNumber, cardBalance);
        }


        public int NumberCard
        {
            get { return cardNumber; }
        }

        public int BalanceCard
        {
            get { return cardBalance; }
        }

    }
}

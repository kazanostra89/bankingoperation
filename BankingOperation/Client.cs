﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingOperation
{
    class Client
    {
        private string name;
        private string surname;
        private int clientID;
        private int deposit;
        private List<BankCard> cards;

        public Client(string name, string surname, int deposit, int clientID)
        {
            this.name = name;
            this.surname = surname;
            this.clientID = clientID;
            this.deposit = deposit;

            cards = new List<BankCard>();
        }

        public void AddNewCard(BankCard bankCard)
        {
            cards.Add(bankCard);
        }

        public void RemoveCard(int numberCard)
        {
            cards.RemoveAt(cards.FindIndex(item => item.NumberCard == numberCard));
        }

        public void DepositOperation(int money, Remittance operation)
        {
            switch (operation)
            {
                case Remittance.Replenish:
                    deposit += money;
                    break;
                case Remittance.Withdrawal:
                    deposit -= money;
                    break;
            }
        }

        public bool CheckCardByNumber(int numberCard)
        {
            return cards.Exists(item => item.NumberCard == numberCard);
        }

        public BankCard GetCardByNumber(int numberCard)
        {
            return cards.Find(item => item.NumberCard == numberCard);
        }

        public void PrintCardByNumber(int cardNumber)
        {
            Console.WriteLine(cards.Find(item => item.NumberCard == cardNumber).PrintInfoCardString());
        }

        public string PrintInfoClientString()
        {
            return "ID: " + clientID + "\nИмя: " + name + "\nФамилия: " + surname + "\nСчет: " + deposit + "\nКол. карт: " + cards.Count + "\n" +  new string('*', 15);
        }

        public void PrintALLCards()
        {
            Console.WriteLine("{0,-9}{1,-7}", "Номер", "Баланс");

            for (int i = 0; i < cards.Count; i++)
            {
                cards[i].PrintCardByList();
            }
        }

        public void PrintAllCardsForMoney()
        {
            Console.WriteLine("{0,-9}{1,-7}", "Номер", "Баланс");

            foreach (BankCard item in cards.FindAll(card => card.BalanceCard > 0))
            {
                item.PrintCardByList();
            }
        }

        public bool SearchZeroMoneyCards()
        {
            for (int i = 0; i < cards.Count; i++)
            {
                if (cards[i].BalanceCard > 0)
                {
                    return false;
                }
            }

            return true;
        }
        
        public List<int> GetListNumbersCardsClient()
        {
            return cards.ConvertAll(item => item.NumberCard);
        }


        public int CountCards
        {
            get { return cards.Count; }
        }

        public int Deposit
        {
            get { return deposit; }
        }

        public BankCard this[int index]
        {
            get { return cards[index]; }
        }

    }
}

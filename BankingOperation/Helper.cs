﻿using System;

namespace BankingOperation
{
    public enum StatusesCards
    {
        Valid = 0,
        Invalid = 1
    }

    public enum Remittance
    {
        Replenish = 0,
        Withdrawal = 1
    }

    public struct BankSettings
    {
        public const int MinOpenCard = 10;
        public const int MinOpenDeposit = 50;
    } 

    static class Helper
    {
        public static int InputIntMinMax(string message, int min, int max)
        {
            int number;
            bool check;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false || number < min || number > max);

            return number;
        }

        public static int InputInt(string message)
        {
            int number;
            bool check;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false);

            return number;
        }

        public static string InputStringOnlyRusChar(string message)
        {
            string str;
            bool noRusChar;
            int counterRusChar;

            do
            {
                noRusChar = true;
                counterRusChar = 0;

                Console.Write(message);
                str = Console.ReadLine();

                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] >= 1040 && str[i] <= 1103)   //только русские заглавные и строчные символы
                    {
                        noRusChar = false;
                        counterRusChar++;                   //блокирует обход если первая русская, а остальные символы другие
                    }
                }

            } while (str == "" || noRusChar || counterRusChar != str.Length);

            return str;
        }

        public static int ConvertToInt(string value)
        {
            return Convert.ToInt32(value);
        }

        public static StatusesCards ConverToStatusesCards(string value)
        {
            if (value.Contains("Valid"))
            {
                return StatusesCards.Valid;
            }

            return StatusesCards.Invalid;
        }
    }
}

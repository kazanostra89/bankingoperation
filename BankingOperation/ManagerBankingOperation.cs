﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

namespace BankingOperation
{
    class ManagerBankingOperation
    {
        private Bank bank;
        private Random rnd;

        public ManagerBankingOperation()
        {
            bank = new Bank();
            rnd = new Random();
        }

        private void PrintMenuStart()
        {
            Console.WriteLine("Вас приветствует " + bank.NameBank + "\n");
            Console.WriteLine("1. Добавление нового клиента");
            Console.WriteLine("2. Выпуск карты, только для клиентов");
            Console.WriteLine("3. Закрытие карты, только для клиентов");
            Console.WriteLine("4. Перевести средства с карты на карту");
            Console.WriteLine("5. Удаление клиента");
            Console.WriteLine("6. Пополнение личного счета");
            Console.WriteLine("7. Отчеты по выпущенным банковским картам");
            Console.WriteLine("0. Завершить операции\n");
        }

        private void PrintMenuCaseOperation()
        {
            Console.WriteLine("\n1. Начать/повторить операцию");
            Console.WriteLine("0. Завершить операцию\n");
        }

        private void PrintMenuCaseTwoThree()
        {
            Console.WriteLine("Вы не являетесь клиентом нашего банка!");
            Console.WriteLine("Операция невозможна");
            Console.ReadKey();
        }

        private void PrintMenuReport()
        {
            Console.WriteLine("1. Показать все выданные банком карты");
            Console.WriteLine("2. Показать информацию по номеру карты");
            Console.WriteLine("3. Фильтрация карт по статусу действия");
            Console.WriteLine("4. Сохранить список всех карт из БД в файл");
            Console.WriteLine("5. Загрузить список карт из файла в БД");
            Console.WriteLine("0. Вернуться в главное меню\n");
        }

        public void Start()
        {
            bool start = true;
            int selectMenuItem, checkIdClient;

            while (start)
            {
                Console.Clear();
                PrintMenuStart();

                selectMenuItem = Helper.InputIntMinMax("Выберите операцию: ", 0, 7);

                switch (selectMenuItem)
                {
                    case 1:
                        #region 1
                        Console.WriteLine("Ввод персональных данных клиента: ");

                        string surname = Helper.InputStringOnlyRusChar("> введите фамилию: ");
                        string name = Helper.InputStringOnlyRusChar("> введите имя: ");
                        int deposit = Helper.InputIntMinMax("> введите сумму счета (>=" + BankSettings.MinOpenDeposit + "): ", BankSettings.MinOpenDeposit, Int32.MaxValue);

                        Console.Clear();

                        bank.AddNewClient(name, surname, deposit);
                        Console.ReadKey();
                        #endregion
                        break;
                    case 2:
                        #region 2
                        if (bank.CountClientsBank == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("Список клиентов пуст!\nВы будете возвращены в главное меню");
                            Console.ReadKey();
                            break;
                        }

                        checkIdClient = Helper.InputInt("Введите Ваш персональный номер: ");

                        if (bank.CheckClienByID(checkIdClient))
                        {
                            Client currentClient = bank.GetClientByID(checkIdClient);

                            bool startOperation = true;

                            Console.WriteLine(currentClient.PrintInfoClientString());

                            while (startOperation)
                            {
                                if (currentClient.Deposit < BankSettings.MinOpenCard)
                                {
                                    Console.WriteLine("\nВнимание! Нажмите клавишу для продолжения");
                                    Console.ReadKey();
                                    Console.Clear();
                                    Console.WriteLine("Баланс Вашего счета = " + currentClient.Deposit + "\nКарту можно открыть от суммы " + BankSettings.MinOpenCard + "\nВы будете возвращены в главное меню");
                                    break;
                                }

                                PrintMenuCaseOperation();

                                selectMenuItem = Helper.InputIntMinMax("Выберите пункт меню: ", 0, 1);

                                switch (selectMenuItem)
                                {
                                    case 1:
                                        Console.Clear();
                                        Console.WriteLine("Баланс Вашего счета: " + currentClient.Deposit);

                                        int balanseCard = Helper.InputIntMinMax("Введите размер вносимых денежных средств (>=" + BankSettings.MinOpenCard + "): ", 10, currentClient.Deposit);

                                        currentClient.DepositOperation(balanseCard, Remittance.Withdrawal); //перевод денег со счета на карту

                                        int numberNewCard = bank.GetNewNumberCard(rnd);

                                        currentClient.AddNewCard(new BankCard(numberNewCard, balanseCard));

                                        Console.Clear();
                                        Console.WriteLine("Поздравляем, ваша карта готова!!!");

                                        currentClient.PrintCardByNumber(numberNewCard);
                                        break;
                                    case 0:
                                        Console.Clear();
                                        Console.WriteLine("Список всех Ваших карт, для продолжения нажмите клавишу!\n");
                                        currentClient.PrintALLCards();
                                        startOperation = false;
                                        break;
                                }

                            }
                        }
                        else
                        {
                            PrintMenuCaseTwoThree();
                        }

                        Console.ReadKey();
                        #endregion
                        break;
                    case 3:
                        #region 3
                        if (bank.CountClientsBank == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("Список клиентов пуст!\nВы будете возвращены в главное меню");
                            Console.ReadKey();
                            break;
                        }

                        checkIdClient = Helper.InputInt("Введите Ваш персональный номер: ");

                        if (bank.CheckClienByID(checkIdClient))
                        {
                            Client currentClient = bank.GetClientByID(checkIdClient);

                            if (currentClient.CountCards == 0)
                            {
                                Console.Clear();
                                Console.WriteLine("У Вас нет действующих карт!");
                                Console.WriteLine("Нажмите клавишу для выхода");
                                Console.ReadKey();
                                break;
                            }

                            bool startRemove = true;

                            while (startRemove)
                            {
                                Console.Clear();

                                if (currentClient.CountCards < 2)
                                {
                                    Console.WriteLine("У Вас одна карта!\nПродолжение операции невозможно!");
                                    #region Закрытие единственной карты
                                    Console.Write("\nДля закрытия единственной карты введите (Да): ");
                                    string question = Console.ReadLine();

                                    if (question.Contains("Да"))
                                    {
                                        BankCard lastCard = currentClient[0];

                                        Console.Clear();
                                        Console.WriteLine("Ваш баланс = " + currentClient.Deposit);
                                        Console.WriteLine("Баланс карты " + lastCard.NumberCard + " = " + lastCard.BalanceCard);
                                        Console.WriteLine("\nPress Enter");
                                        Console.ReadKey();

                                        currentClient.DepositOperation(lastCard.BalanceCard, Remittance.Replenish);
                                        lastCard.CashOperation(lastCard.BalanceCard, Remittance.Withdrawal);

                                        Console.Clear();
                                        Console.WriteLine("Перевод средст с карты на Ваш счет произошел успешно!");
                                        Console.WriteLine("Ваш баланс = " + currentClient.Deposit);
                                        Console.WriteLine("Баланс карты " + lastCard.NumberCard + " = " + lastCard.BalanceCard);

                                        Console.WriteLine("\nPress Enter");
                                        Console.ReadKey();
                                        Console.WriteLine("\nЗакрытие карты - успешно!\n");

                                        bank.SetInvalidStatusCard(lastCard.NumberCard);
                                        currentClient.RemoveCard(lastCard.NumberCard);
                                    }
                                    #endregion
                                    Console.WriteLine("Нажмите клавишу для выхода");
                                    break;
                                }

                                Console.WriteLine(currentClient.PrintInfoClientString());
                                Console.WriteLine("Список ваших карт:\n");
                                currentClient.PrintALLCards();

                                PrintMenuCaseOperation();

                                selectMenuItem = Helper.InputIntMinMax("Выберите пункт меню: ", 0, 1);

                                switch (selectMenuItem)
                                {
                                    case 1:
                                        int numInpCard;

                                        numInpCard = CheckCardForClientByNumber(currentClient, "Введите номер карты для закрытия: ");

                                        if (currentClient.GetCardByNumber(numInpCard).BalanceCard > 0)
                                        {
                                            int numInpCardWhere, transfer;

                                            do
                                            {
                                                numInpCardWhere = CheckCardForClientByNumber(currentClient, "Введите номер карты для перевода остатка с карты: ");

                                            } while (numInpCardWhere == numInpCard);

                                            BankCard tempCardFrom, tempCardWhere;

                                            tempCardFrom = currentClient.GetCardByNumber(numInpCard);
                                            tempCardWhere = currentClient.GetCardByNumber(numInpCardWhere);
                                            transfer = tempCardFrom.BalanceCard;

                                            tempCardWhere.CashOperation(transfer, Remittance.Replenish);
                                            tempCardFrom.CashOperation(transfer, Remittance.Withdrawal);

                                        }

                                        bank.SetInvalidStatusCard(numInpCard);

                                        currentClient.RemoveCard(numInpCard);
                                        break;
                                    case 0:
                                        startRemove = false;
                                        break;
                                }
                            }
                        }
                        else
                        {
                            PrintMenuCaseTwoThree();
                        }

                        Console.ReadKey();
                        #endregion
                        break;
                    case 4:
                        #region 4
                        if (bank.CountClientsBank < 2)
                        {
                            Console.Clear();
                            Console.WriteLine("Операция не возможна, у банка менее двух клиентов!");
                            Console.ReadKey();
                            break;
                        }

                        bank.PrintClientsBankAllList();

                        Client clientFrom, clientWhere;
                        int checkIdFrom, checkIdWhere;

                        checkIdFrom = CheckClientForBankById("Укажите клиента, от которого осуществляется перевод: ");
                        checkIdWhere = CheckClientForBankById("Укажите клиента, которому осуществляется перевод: ");

                        clientFrom = bank.GetClientByID(checkIdFrom);
                        clientWhere = bank.GetClientByID(checkIdWhere);

                        if (CheckClientsZoroMoneyCards(clientFrom, clientWhere))
                        {
                            Console.ReadKey();
                            break;
                        }

                        Console.Clear();
                        Console.WriteLine(clientFrom.PrintInfoClientString().Replace('*', ' '));
                        clientFrom.PrintAllCardsForMoney();
                        Console.WriteLine(new string('*', 25));
                        Console.WriteLine(clientWhere.PrintInfoClientString().Replace('*', ' '));
                        clientWhere.PrintAllCardsForMoney();
                        Console.WriteLine(new string('*', 25));

                        int numCardFrom, numCardWhere;
                        BankCard cardFrom, cardWhere;

                        numCardFrom = CheckCardForClientByNumber(clientFrom, "Введите номер карты отправителя: ");
                        cardFrom = clientFrom.GetCardByNumber(numCardFrom);

                        int transferTools = Helper.InputIntMinMax("Введите желаемую сумму перевода: ", 0, cardFrom.BalanceCard);

                        numCardWhere = CheckCardForClientByNumber(clientWhere, "Введите номер карты получателя: ");
                        cardWhere = clientWhere.GetCardByNumber(numCardWhere);

                        cardFrom.CashOperation(transferTools, Remittance.Withdrawal);
                        cardWhere.CashOperation(transferTools, Remittance.Replenish);

                        clientFrom.PrintAllCardsForMoney();
                        clientWhere.PrintAllCardsForMoney();

                        Console.ReadKey();
                        #endregion
                        break;
                    case 5:
                        #region 5
                        if (bank.CountClientsBank == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("Список клиентов пуст!\nВы будете возвращены в главное меню");
                            Console.ReadKey();
                            break;
                        }

                        bank.PrintClientsBankAllList();

                        checkIdClient = CheckClientForBankById("Введите персональный номер клиента: ");

                        Client removeClient = bank.GetClientByID(checkIdClient);

                        Console.Clear();
                        Console.WriteLine(removeClient.PrintInfoClientString());

                        if (removeClient.CountCards > 0)
                        {
                            Console.WriteLine("Происходит закрытие всех карт клиента...");

                            foreach (int numberCard in removeClient.GetListNumbersCardsClient())
                            {
                                bank.SetInvalidStatusCard(numberCard);
                            }

                            Console.WriteLine("Нажмите любую клавишу для продолжения операции!");
                            Console.ReadKey();
                        }

                        Console.Clear();
                        bank.RemoveClient(checkIdClient);
                        Console.WriteLine("Клиент успешно удален из БД");
                        bank.PrintClientsBankAllList();

                        Console.ReadKey();
                        #endregion
                        break;
                    case 6:
                        #region 6
                        if (bank.CountClientsBank == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("Список клиентов пуст!\nВы будете возвращены в главное меню");
                            Console.ReadKey();
                            break;
                        }

                        checkIdClient = Helper.InputInt("Введите Ваш персональный номер: ");

                        if (bank.CheckClienByID(checkIdClient))
                        {
                            Console.Clear();

                            Client currentClient = bank.GetClientByID(checkIdClient);

                            Console.WriteLine(currentClient.PrintInfoClientString());

                            Console.WriteLine("Минимальная сумма пополнения = " + BankSettings.MinOpenDeposit);
                            int inputDeposit = Helper.InputIntMinMax("\nВведите сумму для пополнения счета: ", BankSettings.MinOpenDeposit, Int32.MaxValue);

                            currentClient.DepositOperation(inputDeposit, Remittance.Replenish);

                            Console.Clear();
                            Console.WriteLine("Счет успешно пополнена на " + inputDeposit);
                            Console.WriteLine("\nВаш баланс равен: " + currentClient.Deposit);
                        }
                        else
                        {
                            PrintMenuCaseTwoThree();
                        }

                        Console.ReadKey();
                        #endregion
                        break;
                    case 7:
                        #region 7
                        if (bank.CountNumberCards == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("\nСписок карт в БД пуст!\nВы будете возвращены в главное меню");
                            Console.ReadKey();
                            break;
                        }

                        bool startReport = true;

                        while (startReport)
                        {
                            Console.Clear();
                            PrintMenuReport();

                            selectMenuItem = Helper.InputIntMinMax("Выберите операцию: ", 0, 5);

                            switch (selectMenuItem)
                            {
                                case 1:
                                    Console.Clear();
                                    bank.PrintNumbersCardAllList();
                                    Console.ReadKey();
                                    break;
                                case 2:
                                    Console.Clear();
                                    bank.PrintNumbersCardAllList();
                                    int cardNumber;

                                    do
                                    {
                                        cardNumber = Helper.InputInt("Введите номер карты из списка: ");

                                    } while (bank.CheckCardByNumber(cardNumber) != true);

                                    Console.Clear();
                                    bank.PrintNumbersCardByValue(cardNumber);
                                    Console.ReadKey();
                                    break;
                                case 3:
                                    Console.Clear();
                                    bank.PrintNumbersCardAllList();

                                    Console.WriteLine("Сатусы карт:\n0 = Valid/Действует\n1 = Invalid/Закрыта");
                                    int inputStatuses = Helper.InputIntMinMax("Введите соответствующий статусу код: ", 0, 1);
                                    
                                    Console.Clear();
                                    bank.PrintNumbersCardByValue((StatusesCards)inputStatuses);
                                    Console.ReadKey();
                                    break;
                                case 4:
                                    Console.Clear();
                                    Console.WriteLine("Выполняется копирование БД в файл [ListCardDB.txt]:\nЖдите...");
                                    Console.WriteLine("Press to key: Enter");
                                    Console.ReadKey();

                                    bank.SaveListCardsToFile();

                                    if (File.Exists(@"C:\Users\Родион\Desktop\123\ListCardDB.txt"))
                                    {
                                        Console.WriteLine("Копирование выполнено успешно!");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Ошибка копирования, повторите операцию!");
                                    }

                                    Console.ReadKey();
                                    break;
                                case 5:
                                    bank.ExecListCardsFromFile();
                                    break;
                                case 0:
                                    startReport = false;
                                    break;
                            }

                        }
                        Console.ReadKey();
                        #endregion
                        break;
                    case 0:
                        start = false;
                        break;
                }
            }
        }


        private int CheckClientForBankById(string message)
        {
            int checkID;

            do
            {
                checkID = Helper.InputInt(message);

            } while (bank.CheckClienByID(checkID) != true);

            return checkID;
        }

        private int CheckCardForClientByNumber(Client client, string message)
        {
            int numberCard;

            do
            {
                numberCard = Helper.InputInt(message);

            } while (client.CheckCardByNumber(numberCard) != true);

            return numberCard;
        }

        private bool CheckClientsZoroMoneyCards(Client clientFrom, Client clientWhere)
        {
            bool checkZeroFrom, checkZeroWhere;

            checkZeroFrom = clientFrom.SearchZeroMoneyCards();
            checkZeroWhere = clientWhere.SearchZeroMoneyCards();

            if (checkZeroFrom == true && checkZeroWhere == true)
            {
                Console.Clear();
                Console.WriteLine("У обоих клиентов нет карт, либо балансы на их картах равны 0!");
                return true;
            }
            else if (checkZeroFrom == true)
            {
                Console.Clear();
                Console.WriteLine("Операция невозможна!\nУ переводящего клиента нет карт или денежных средств!");
                return true;
            }
            else if (clientWhere.CountCards == 0)
            {
                Console.Clear();
                Console.WriteLine("Операция невозможна!\nУ клиента адресата нет карт для перевода!");
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
